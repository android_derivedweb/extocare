package com.fashion.extocare.Model;

public class ItemListModel {

    private String price;
    private String quantity;
    private String name;
    private String sr_no;
    private String id;
    private String base_price;
    private String priceWithSR;

    public String getPriceWithSR() {
        return priceWithSR;
    }

    public void setPriceWithSR(String priceWithSR) {
        this.priceWithSR = priceWithSR;
    }

    public String getBase_price() {
        return base_price;
    }

    public void setBase_price(String base_price) {
        this.base_price = base_price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSr_no() {
        return sr_no;
    }

    public void setSr_no(String sr_no) {
        this.sr_no = sr_no;
    }
}
