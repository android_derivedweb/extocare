package com.fashion.extocare.Model;

public class ApiModel {

    private String id;
    private String nameAr;
    private String nameEn;
    private String price;
    private String isCountable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIsCountable() {
        return isCountable;
    }

    public void setIsCountable(String isCountable) {
        this.isCountable = isCountable;
    }

}
