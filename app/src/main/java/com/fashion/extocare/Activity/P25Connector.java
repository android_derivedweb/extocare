package com.fashion.extocare.Activity;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.fashion.extocare.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.UUID;

public class P25Connector {
	public static BluetoothSocket mSocket;
	private OutputStream mOutputStream;
	
	private ConnectTask mConnectTask;
	private P25ConnectionListener mListener;
	
	private boolean mIsConnecting = false;
	
	private static final String TAG = "P25";	
	private static final String SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
	private int mWidth;
	private int mHeight;
	private String mStatus;
	private BitSet dots;

	private final byte ESC_CHAR = 0x1B;
	private final byte[] PRINTER_SET_LINE_SPACE_24 = new byte[]{ESC_CHAR, 0x33, 24};
	private static final byte[] PRINTER_SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33};
	private final byte BYTE_LF = 0xA;


	public P25Connector(P25ConnectionListener listener) {
		mListener = listener;
	}
	
	public boolean isConnecting() {
		return mIsConnecting;
	}
	
	public boolean isConnected() {
		return (mSocket == null) ? false : true;
	}
	
	public void connect(BluetoothDevice device) throws P25ConnectionException {
		if (mIsConnecting && mConnectTask != null) {
			throw new P25ConnectionException("Connection in progress");
		}
		
		if (mSocket != null) {
			throw new P25ConnectionException("Socket already connected");
		}
		
		(mConnectTask = new ConnectTask(device)).execute();
	}
	
	public void disconnect() throws P25ConnectionException {
		if (mSocket == null) {
			throw new P25ConnectionException("Socket is not connected");
		}
		
		try {
			mSocket.close();
			
			mSocket = null;
			
			mListener.onDisconnected();
		} catch (IOException e) {
			throw new P25ConnectionException(e.getMessage());
		}
	}
	
	public void cancel() throws P25ConnectionException {
		if (mIsConnecting && mConnectTask != null) {
			mConnectTask.cancel(true);
		} else {
			throw new P25ConnectionException("No connection is in progress");
		}
	}
	
	public void sendData(Bitmap bmp,byte[] mGroup2,Bitmap mGroup3,byte[] mGroup4) throws P25ConnectionException {
		if (mSocket == null) {
			throw new P25ConnectionException("Socket is not connected, try to call connect() first");
		}
			
		try {
			printImage(bmp);
			mOutputStream.write(mGroup2);
			printImage(mGroup3);
			mOutputStream.write(mGroup4);

			mOutputStream.flush();

			Log.e(TAG, StringUtil.byteToString(mGroup2));
			Log.e(TAG, StringUtil.byteToString(mGroup4));
		} catch(Exception e) {
			throw new P25ConnectionException(e.getMessage());
		}
	}



	void printImage(Bitmap bitmap) {

		//int width = bitmap.getWidth();
		int width =75;
		int height =75;
		//int height = bitmap.getHeight();

		final byte[] controlByte = {(byte) (0x00ff & width), (byte) ((0xff00 & width) >> 8)};
		int[] pixels = new int[width * height];

		bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

		final int BAND_HEIGHT = 24;

		// Bands of pixels are sent that are 8 pixels high.  Iterate through bitmap
		// 24 rows of pixels at a time, capturing bytes representing vertical slices 1 pixel wide.
		// Each bit indicates if the pixel at that position in the slice should be dark or not.
		for (int row = 0; row < height; row += BAND_HEIGHT) {
			ByteArrayOutputStream imageData = getOutputStream();

			writeToPrinterBuffer(imageData, PRINTER_SET_LINE_SPACE_24);

			// Need to send these two sets of bytes at the beginning of each row.
			writeToPrinterBuffer(imageData, PRINTER_SELECT_BIT_IMAGE_MODE);
			writeToPrinterBuffer(imageData, controlByte);

			// Columns, unlike rows, are one at a time.
			for (int col = 0; col < width; col++) {

				byte[] bandBytes = {0x0, 0x0, 0x0};

				// Ugh, the nesting of forloops.  For each starting row/col position, evaluate
				// each pixel in a column, or "band", 24 pixels high.  Convert into 3 bytes.
				for (int rowOffset = 0; rowOffset < 8; rowOffset++) {

					// Because the printer only maintains correct height/width ratio
					// at the highest density, where it takes 24 bit-deep slices, process
					// a 24-bit-deep slice as 3 bytes.
					int[] pixelSlice = new int[3];
					int pixel2Row = row + rowOffset + 8;
					int pixel3Row = row + rowOffset + 16;

					// If we go past the bottom of the image, just send white pixels so the printer
					// doesn't do anything.  Everything still needs to be sent in sets of 3 rows.
					pixelSlice[0] = bitmap.getPixel(col, row + rowOffset);
					pixelSlice[1] = (pixel2Row >= bitmap.getHeight()) ?
							Color.WHITE : bitmap.getPixel(col, pixel2Row);
					pixelSlice[2] = (pixel3Row >= bitmap.getHeight()) ?
							Color.WHITE : bitmap.getPixel(col, pixel3Row);

					boolean[] isDark = {pixelSlice[0] == Color.BLACK,
							pixelSlice[1] == Color.BLACK,
							pixelSlice[2] == Color.BLACK};

					// Towing that fine line between "should I forloop or not".  This will only
					// ever be 3 elements deep.
					if (isDark[0]) bandBytes[0] |= 1 << (7 - rowOffset);
					if (isDark[1]) bandBytes[1] |= 1 << (7 - rowOffset);
					if (isDark[2]) bandBytes[2] |= 1 << (7 - rowOffset);
				}
				writeToPrinterBuffer(imageData, bandBytes);
			}
			addLineFeed(imageData, 1);
			print(imageData);
		}
	}
	private final byte[] PRINTER_PRINT_AND_FEED = {0x1B, 0x64};


	private void addLineFeed(ByteArrayOutputStream printerBuffer, int numLines) {
		try {
			if (numLines <= 1) {
				printerBuffer.write(BYTE_LF);
			} else {
				printerBuffer.write(PRINTER_PRINT_AND_FEED);
				printerBuffer.write(numLines);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void print(ByteArrayOutputStream output) {
		try {
			writeUartData(output.toByteArray());

		} catch (IOException e) {
			Log.d(TAG, "IO Exception while printing.", e);
		}
	}

	private synchronized void writeUartData(byte[] data) throws IOException {

		// In the case of writing images, let's assume we shouldn't send more than 400 bytes
		// at a time to avoid buffer overrun - At which point the thermal printer tends to
		// either lock up or print garbage.
		final int DEFAULT_CHUNK_SIZE = 400;

		byte[] chunk = new byte[DEFAULT_CHUNK_SIZE];
		ByteBuffer byteBuffer = ByteBuffer.wrap(data);
		while (byteBuffer.remaining() > DEFAULT_CHUNK_SIZE) {
			byteBuffer.get(chunk);
			mOutputStream.write(chunk);
			try {
				this.wait(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (byteBuffer.hasRemaining()) {
			byte[] lastChunk = new byte[byteBuffer.remaining()];
			byteBuffer.get(lastChunk);
			mOutputStream.write(lastChunk);
		}
	}


	private void writeToPrinterBuffer(ByteArrayOutputStream printerBuffer, byte[] command) {
		try {
			printerBuffer.write(command);
		} catch (IOException e) {
			Log.d(TAG, "IO Exception while writing printer data to buffer.", e);
		}
	}

	private ByteArrayOutputStream getOutputStream() {
		return new ByteArrayOutputStream();
	}




	public interface P25ConnectionListener {
		public abstract void onStartConnecting();
		public abstract void onConnectionCancelled();
		public abstract void onConnectionSuccess();
		public abstract void onConnectionFailed(String error);
		public abstract void onDisconnected();
	}
	
	public class ConnectTask extends AsyncTask<URL, Integer, Long> {
		BluetoothDevice device;
		String error = "";
		
		public ConnectTask(BluetoothDevice device) {
			this.device = device;
		}
		
		protected void onCancelled() {
			mIsConnecting = false;
			
			mListener.onConnectionCancelled();
		}
		
    	protected void onPreExecute() {
    		mListener.onStartConnecting();
    		
    		mIsConnecting = true;
    	}
    
        protected Long doInBackground(URL... urls) {         
            long result = 0;
            
            try {
            	mSocket			= device.createRfcommSocketToServiceRecord(UUID.fromString(SPP_UUID));
            	
            	mSocket.connect(); 
            	
            	mOutputStream	= mSocket.getOutputStream();
            	
            	result = 1;
            } catch (IOException e) { 
            	e.printStackTrace();
            	
            	error = e.getMessage();
            }
            
            return result;
        }

        protected void onProgressUpdate(Integer... progress) {              	
        }

        protected void onPostExecute(Long result) {        	
        	mIsConnecting = false;
        	
        	if (mSocket != null && result == 1) {
        		mListener.onConnectionSuccess();
        	} else {
        		mListener.onConnectionFailed("Connection failed " + error);
        	}
        }
    }
}