package com.fashion.extocare.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fashion.extocare.API.UserSession;
import com.fashion.extocare.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {


    private UserSession session;
    private EditText mUserName;
    private EditText mPassword;
    private RequestQueue mRequestqueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        session = new UserSession(Login.this);
        mUserName = findViewById(R.id.mUserName);
        mPassword = findViewById(R.id.mPassword);
        mRequestqueue = Volley.newRequestQueue(this);
        findViewById(R.id.mLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mUserName.getText().toString().isEmpty()){
                    Toast.makeText(Login.this,"Please Enter User Name",Toast.LENGTH_SHORT).show();
                }else  if(mPassword.getText().toString().isEmpty()){
                    Toast.makeText(Login.this,"Please Enter User Password",Toast.LENGTH_SHORT).show();
                }else {
                    mUserLogin(mUserName.getText().toString(),mPassword.getText().toString());
                }

                
            }
        });

    }



    private void mUserLogin(String mUserName, String mPassword) {

        final KProgressHUD progressDialog = KProgressHUD.create(Login.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        Map<String, String> postParam= new HashMap<String, String>();
        postParam.put("username", mUserName);
        postParam.put("password", mPassword);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                session.BASEURL + "Authenticate", new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            session.createLoginSession(
                                    response.getString("id"),
                                    response.getString("nameAr"),
                                    response.getString("nameEn"),
                                    response.getString("nameEn"),
                                    response.getJSONObject("branch").getString("id"),
                                    response.getJSONObject("branch").getString("name"),
                                    response.getJSONObject("branch").getString("vat_number"),
                                    response.getJSONObject("branch").getString("location"),
                                    response.getJSONObject("branch").getString("display_name"),
                                    response.getJSONObject("branch").getString("email"),
                                    response.getJSONObject("branch").getString("website"),
                                    response.getJSONObject("branch").getString("contact"),
                                    response.getJSONObject("branch").getString("city"),
                                    response.getJSONObject("branch").getJSONObject("company").getString("id"),
                                    response.getJSONObject("branch").getJSONObject("company").getString("name"),
                                    response.getJSONObject("branch").getJSONObject("company").getString("displayNameAr"),
                                    response.getString("token")

                            );

                            startActivity(new Intent(Login.this, MainActivity.class));
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("TAG", response.toString());



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", "Error: " + error.getMessage());
                Toast.makeText(Login.this,"Error Code: " + error.networkResponse.statusCode,Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }



        };

        jsonObjReq.setTag("TAG");
        // Adding request to request queue
        mRequestqueue.add(jsonObjReq);

        // Cancelling request
    /* if (queue!= null) {
    queue.cancelAll(TAG);
    } */

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (session.isLoggedIn()){
            startActivity(new Intent(Login.this, MainActivity.class));
            finish();
        }
    }
}