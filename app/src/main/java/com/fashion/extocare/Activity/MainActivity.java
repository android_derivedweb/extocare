package com.fashion.extocare.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fashion.extocare.API.UserSession;
import com.fashion.extocare.Adapter.AdapterCarList;
import com.fashion.extocare.Adapter.AdapterItemsList;
import com.fashion.extocare.Adapter.AdapterList3;
import com.fashion.extocare.Model.ApiModel;
import com.fashion.extocare.Model.BillModel;
import com.fashion.extocare.Model.ItemListModel;
import com.fashion.extocare.Model.ServiceModel3;
import com.fashion.extocare.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private UserSession session;
    private RequestQueue mRequestqueue;

    private AdapterCarList adapterCarList;
    private AdapterList3 adapterList3;
    private RecyclerView recycler1, recycler2, recycler3, recDetailItems;

    private int[] images = {R.drawable.suv, R.drawable.suv_mini, R.drawable.hatchback, R.drawable.sedan};
    private String [] idArray1 = {"1", "2", "3", "4"};
    public static String item1 = "";

    // for recylcer 2
    private int[] serviceImage = {R.drawable.service1, R.drawable.service2, R.drawable.service3};
    private String [] idArray2 = {"3", "2", "1"};
    public static String item2 = "";

    // for recycler 3
    private int[] serviceImage3 = {R.drawable.last1, R.drawable.last2, R.drawable.last3, R.drawable.last4};
    private ArrayList<ServiceModel3> serviceModelArrayList3 = new ArrayList<>();
    private String [] idArray3 = {"303", "301", "304", "151"};

    private LinearLayout showDetail;
    private ArrayList<ItemListModel> detailArrayList = new ArrayList<>();
    private AdapterItemsList adapterItemsList;

    private LinearLayout cashLinear, cardLinear, cash_cardLinear, linerList;
    private RelativeLayout collapseLinear;


    private ArrayList<ApiModel> apiModelArrayList = new ArrayList<>();

    // for get group 1 and gruop 2 values
    private String g1g2 = "1";

    private TextView listSizeText, totalItemsBottom;

    private String tax = "";

    private TextView amountText, taxText, totalAmountText;

    private float totalTaxAmount = 0;
    private float amount = 0;

    // for payment type
    private String type_Payment = "";
    private String cash_Payment = "";
    private String card_Payment = "";


    private Button mConnectBtn;
    private Button mEnableBtn;
    private TextView btnPrint;
    private Spinner mDeviceSp;

    private ProgressDialog mProgressDlg;
    private ProgressDialog mConnectingDlg;

    private BluetoothAdapter mBluetoothAdapter;

    private P25Connector mConnector;

    private ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();



    // for fianal responce for print data
    private String logoBill = "";
    private String companyNameBill = "";
    private String branchAddressBill = "";
    private String companyVatNumberBill = "";
    private String qrCodeBill = "";
    private String billNoBill = "";
    private String dateBill = "";
    private String timeBill = "";
    private String cashiermanBill = "";
    private String paymentMethodBill = "";
    private String amountBill = "";
    private String taxBill = "";
    private String totalBill = "";
    private String branchEmailBill = "";
    private String branchWebsiteBill = "";
    private String branchPhoneBill = "";

    private ArrayList<BillModel> listOfServicesBill = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);  //  set status text dark

        session = new UserSession(MainActivity.this);
        mRequestqueue = Volley.newRequestQueue(this);
        initializeAll();//initialize all variable


        showDetail = findViewById(R.id.showDetail);
        cashLinear = findViewById(R.id.cashLinear);
        cardLinear = findViewById(R.id.cardLinear);
        linerList = findViewById(R.id.linerList);
        collapseLinear = findViewById(R.id.collapseLinear);
        cash_cardLinear = findViewById(R.id.cash_cardLinear);

        listSizeText = findViewById(R.id.listSizeText);
        totalItemsBottom = findViewById(R.id.totalItemsBottom);
        amountText = findViewById(R.id.amountText);
        taxText = findViewById(R.id.taxText);
        totalAmountText = findViewById(R.id.totalAmountText);


        showDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (detailArrayList.size() != 0) {
                    linerList.setVisibility(View.VISIBLE);
                    showDetail.setVisibility(View.GONE);
                }
            }
        });

        collapseLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linerList.setVisibility(View.GONE);
                showDetail.setVisibility(View.VISIBLE);
            }
        });

        for (int i = 0; i < idArray3.length; i++){
            ServiceModel3 serviceModel = new ServiceModel3();
            serviceModel.setIsSelect("false");
            serviceModel.setQuantity("1");
            serviceModel.setId(idArray3[i]);
            serviceModelArrayList3.add(serviceModel);
        }


        LinearLayoutManager linearLayout1 = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        linearLayout1.setReverseLayout(true);
        recycler1 = findViewById(R.id.recycler1);
        recycler1.setLayoutManager(linearLayout1);
        adapterCarList = new AdapterCarList(MainActivity.this, images, new AdapterCarList.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                item1 = idArray1[item];

                g1g2 = "";

                g1g2 = "1" + item1 + item2;

                if (!item2.isEmpty()) {
                    for (int i = 0; i < apiModelArrayList.size(); i++) {
                        if (g1g2.equals(apiModelArrayList.get(i).getId())) {

                            if (detailArrayList.isEmpty()) {
                                ItemListModel itemListModel = new ItemListModel();
                                itemListModel.setQuantity("1");
                                itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                                itemListModel.setSr_no("1");
                                itemListModel.setId(g1g2);
                                itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                                Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                                itemListModel.setPrice(String.valueOf(finalPrice));

                                detailArrayList.add(itemListModel);
                            } else {
                                ItemListModel itemListModel = new ItemListModel();
                                itemListModel.setQuantity("1");
                                itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                                itemListModel.setSr_no("1");
                                itemListModel.setId(g1g2);
                                itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                                Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                                itemListModel.setPrice(String.valueOf(finalPrice));

                                detailArrayList.set(0, itemListModel);
                            }


                            listSizeText.setText(String.valueOf(detailArrayList.size()));
                            totalItemsBottom.setText(String.valueOf(detailArrayList.size()));


                            totalTaxAmount = 0;
                            for (int j = 0; j < detailArrayList.size(); j++){
                                totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                            }
                            Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                            totalAmountText.setText("SR " + rounded);

                            amount = 0;
                            amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                            Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                            amountText.setText("SR " + rounded1);

                            Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                            taxText.setText("SR " + roundedTax);
                        }

                    }
                }

                adapterItemsList.notifyDataSetChanged();

            }
        });
        recycler1.setAdapter(adapterCarList);
        recycler1.setHasFixedSize(true);



        LinearLayoutManager linearLayout2 = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        linearLayout2.setReverseLayout(true);
        recycler2 = findViewById(R.id.recycler2);
        recycler2.setLayoutManager(linearLayout2);
        adapterCarList = new AdapterCarList(MainActivity.this, serviceImage, new AdapterCarList.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                if (item1.isEmpty()){
                    Toast.makeText(MainActivity.this, "Please select Group 1!", Toast.LENGTH_SHORT).show();
                    return;
                }
                item2 = idArray2[item];

                g1g2 = "";
                g1g2 = "1" + item1 + item2;

                for (int i = 0; i < apiModelArrayList.size(); i++){
                    if (g1g2.equals(apiModelArrayList.get(i).getId())){

                        if (detailArrayList.isEmpty()) {
                            ItemListModel itemListModel = new ItemListModel();
                            itemListModel.setQuantity("1");
                            itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                            itemListModel.setSr_no("1");
                            itemListModel.setId(g1g2);
                            itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                            Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                            itemListModel.setPrice(String.valueOf(finalPrice));

                            detailArrayList.add(itemListModel);
                        } else {
                            ItemListModel itemListModel = new ItemListModel();
                            itemListModel.setQuantity("1");
                            itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                            itemListModel.setSr_no("1");
                            itemListModel.setId(g1g2);
                            itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                            Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                            itemListModel.setPrice(String.valueOf(finalPrice));

                            detailArrayList.set(0, itemListModel);
                        }

                        totalTaxAmount = 0;
                        for (int j = 0; j < detailArrayList.size(); j++){
                            totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                        }
                        Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        totalAmountText.setText("SR " + rounded);

                        amount = 0;
                        amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                        Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        amountText.setText("SR " + rounded1);

                        Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        taxText.setText("SR " + roundedTax);


                        listSizeText.setText(String.valueOf(detailArrayList.size()));
                        totalItemsBottom.setText(String.valueOf(detailArrayList.size()));

                    }
                }

                adapterItemsList.notifyDataSetChanged();

            }
        });
        recycler2.setAdapter(adapterCarList);





        LinearLayoutManager linearLayout3 = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        linearLayout3.setReverseLayout(true);
        recycler3 = findViewById(R.id.recycler3);
        recycler3.setLayoutManager(linearLayout3);
        adapterList3 = new AdapterList3(MainActivity.this, serviceImage3, serviceModelArrayList3, new AdapterList3.OnItemClickListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onItemClick(int item) {

                if (item2.isEmpty()){
                    Toast.makeText(MainActivity.this, "Please select Group 2!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (serviceModelArrayList3.get(item).getIsSelect().equals("false")) {
                    serviceModelArrayList3.get(item).setIsSelect("true");

                    for (int i = 0; i < apiModelArrayList.size(); i++){

                        if (idArray3[item].equals(apiModelArrayList.get(i).getId())){
                            ItemListModel itemListModel = new ItemListModel();
                            itemListModel.setQuantity("1");
                            itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                            itemListModel.setSr_no(String.valueOf(detailArrayList.size() + 1));
                            itemListModel.setId(idArray3[item]);
                            itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());


                            Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                            itemListModel.setPrice(String.valueOf(finalPrice));

                            detailArrayList.add(itemListModel);
                        }
                    }


                } else {
                    serviceModelArrayList3.get(item).setIsSelect("false");

                    for (int i = 0; i < detailArrayList.size(); i++){
                        if (idArray3[item].equals(detailArrayList.get(i).getId())){
                            detailArrayList.remove(i);

                            for (int j = 0; j < detailArrayList.size(); j++){
                                detailArrayList.get(j).setSr_no(String.valueOf(j+1));
                            }
                        }
                    }

                }

                listSizeText.setText(String.valueOf(detailArrayList.size()));
                totalItemsBottom.setText(String.valueOf(detailArrayList.size()));


                totalTaxAmount = 0;
                for (int j = 0; j < detailArrayList.size(); j++){
                    totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                }
                Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                totalAmountText.setText("SR " + rounded);

                amount = 0;
                amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                amountText.setText("SR " + rounded1);

                Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                taxText.setText("SR " + roundedTax);

                adapterItemsList.notifyDataSetChanged();
                adapterList3.notifyDataSetChanged();

            }
            @Override
            public void onItemClickPlus(int position, String quantity) {

                if (position != 1 && position != 3) {
                    serviceModelArrayList3.get(position).setQuantity(quantity);

                    for (int i = 0; i < detailArrayList.size(); i++){
                        if (idArray3[position].equals(detailArrayList.get(i).getId())){
                            detailArrayList.get(i).setQuantity(quantity);

                            Float finalPrice = Float.parseFloat(quantity) *
                                    (Float.parseFloat(detailArrayList.get(i).getBase_price()) * (1 + (Float.parseFloat(tax))/100));

                            detailArrayList.get(i).setPrice(String.valueOf(finalPrice));

                            Log.e("quanData", quantity + "--" + detailArrayList.get(i).getPrice());

                            adapterItemsList.notifyDataSetChanged();
                        }
                    }

                }

                totalTaxAmount = 0;
                for (int j = 0; j < detailArrayList.size(); j++){
                    totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                }
                Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                totalAmountText.setText("SR " + rounded);

                amount = 0;
                amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                amountText.setText("SR " + rounded1);

                Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                taxText.setText("SR " + roundedTax);
            }

            @Override
            public void onItemClickMinus(int position, String quantity) {
                serviceModelArrayList3.get(position).setQuantity(quantity);

                if (position != 1 && position != 3) {
                    serviceModelArrayList3.get(position).setQuantity(quantity);

                    for (int i = 0; i < detailArrayList.size(); i++){
                        if (idArray3[position].equals(detailArrayList.get(i).getId())){
                            detailArrayList.get(i).setQuantity(quantity);

                            Float finalPrice = Float.parseFloat(quantity) *
                                    (Float.parseFloat(detailArrayList.get(i).getBase_price()) * (1 + (Float.parseFloat(tax))/100));

                            detailArrayList.get(i).setPrice(String.valueOf(finalPrice));

                            Log.e("quanData", quantity + "--" + detailArrayList.get(i).getBase_price());

                            adapterItemsList.notifyDataSetChanged();
                        }
                    }
                }

                totalTaxAmount = 0;
                for (int j = 0; j < detailArrayList.size(); j++){
                    totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                }
                Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                totalAmountText.setText("SR " + rounded);

                amount = 0;
                amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                amountText.setText("SR " + rounded1);

                Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                taxText.setText("SR " + roundedTax);
            }

            @Override
            public void onClickGone(int position, String quantity) {
                serviceModelArrayList3.get(position).setQuantity(quantity);
            }

        });
        recycler3.setAdapter(adapterList3);



       /* findViewById(R.id.click).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("daatta", g1g2 + "--");

                for (int i = 0; i < serviceModelArrayList3.size(); i++){
                    if (serviceModelArrayList3.get(i).getIsSelect().equals("true")) {
                        Log.e("checkData", item1 + "--" + item2 + "--" +
                                serviceModelArrayList3.get(i).getQuantity() + "--" + serviceModelArrayList3.get(i).getId());
                    }
                }
            }
        });*/



        recDetailItems = findViewById(R.id.recDetailItems);
        recDetailItems.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        adapterItemsList = new AdapterItemsList(MainActivity.this, detailArrayList);
        recDetailItems.setAdapter(adapterItemsList);



        cardLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (detailArrayList.size() >= 1) {
                    cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_right));
                    cashLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));
                    cash_cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));

                    btnPrint.setVisibility(View.VISIBLE);

                    card_Payment = totalAmountText.getText().toString().replace("SR ", "");
                    cash_Payment = "0.0";

                    type_Payment = "Credit Card";
                }

            }
        });
        cashLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (detailArrayList.size() >= 1) {
                    cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));
                    cashLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_right));
                    cash_cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));

                    btnPrint.setVisibility(View.VISIBLE);

                    cash_Payment = totalAmountText.getText().toString().replace("SR ", "");
                    card_Payment = "0.0";

                    type_Payment = "Cash";
                }

            }
        });
        cash_cardLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (detailArrayList.size() >= 1) {
                    cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));
                    cashLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));
                    cash_cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_right));

                    btnPrint.setVisibility(View.VISIBLE);
                    type_Payment = "Multipayment";

                    openDialog();
                }

            }
        });

        findViewById(R.id.logOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.logout();

                Intent intent = new Intent(MainActivity.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


              /*  if (!mConnector.isConnected()) {
                    Toast.makeText(MainActivity.this,"Please connect Printer...",Toast.LENGTH_SHORT).show();
                    connect();
                }else {
                }*/

                getPrint();

               /* Log.e("finalValues",
                        "amount " + amountText.getText().toString().replace("SR ", "") + "\n" +
                        "tax " + taxText.getText().toString().replace("SR ", "") + "\n" +
                        "total " + totalAmountText.getText().toString().replace("SR ", "") + "\n" +
                        "paymentMethod " + type_Payment + "\n" +
                        "cash " + cash_Payment + "\n" +
                        "creditCard " + card_Payment + "\n" + object.toString()
                        );*/
            }
        });


        getAllData();



        if (mBluetoothAdapter == null) {
            showUnsupported();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                showDisabled();
            } else {
                showEnabled();

                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if (pairedDevices != null) {
                    mDeviceList.addAll(pairedDevices);

                    updateDeviceList();
                }
            }

            mProgressDlg 	= new ProgressDialog(this);

            mProgressDlg.setMessage("Scanning...");
            mProgressDlg.setCancelable(false);
            mProgressDlg.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    mBluetoothAdapter.cancelDiscovery();
                }
            });

            mConnectingDlg 	= new ProgressDialog(this);

            mConnectingDlg.setMessage("Connecting...");
            mConnectingDlg.setCancelable(false);

            mConnector 		= new P25Connector(new P25Connector.P25ConnectionListener() {

                @Override
                public void onStartConnecting() {
                    mConnectingDlg.show();
                }

                @Override
                public void onConnectionSuccess() {
                    mConnectingDlg.dismiss();

                    showConnected();
                }

                @Override
                public void onConnectionFailed(String error) {
                    mConnectingDlg.dismiss();
                    showToast("Failed");
                }

                @Override
                public void onConnectionCancelled() {
                    mConnectingDlg.dismiss();
                }

                @Override
                public void onDisconnected() {
                    showDisonnected();
                }
            });

            //enable bluetooth
            mEnableBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

                    startActivityForResult(intent, 1000);
                }
            });

            //connect/disconnect
            mConnectBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    connect();
                }
            });
          /*  btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {
                        printMessage();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            });*/


        }



        IntentFilter filter = new IntentFilter();

        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

        registerReceiver(mReceiver, filter);




    }


    public void printMessage() throws UnsupportedEncodingException {


        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
              /*  listOfServicesBill.clear();
                logoBill = "";
                qrCodeBill = "";
                companyNameBill = "";
                branchAddressBill = "";
                companyVatNumberBill = "";
                billNoBill = "";
                dateBill = "";
                timeBill = "";
                cashiermanBill = "";
                paymentMethodBill = "";
                amountBill = "";
                taxBill = "";
                totalBill = "";
                branchEmailBill = "";
                branchWebsiteBill = "";
                branchPhoneBill = "";
                btnPrint.setVisibility(View.GONE);
                type_Payment = "";*/

                item1 = "";
                item2 = "";
                detailArrayList.clear();
                listSizeText.setText("");

                linerList.setVisibility(View.GONE);
                showDetail.setVisibility(View.VISIBLE);

                cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));
                cashLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));
                cash_cardLinear.setBackground(getResources().getDrawable(R.drawable.round_gray_10_two_side_white));

                btnPrint.setVisibility(View.GONE);
                // for payment type
                type_Payment = "";
                cash_Payment = "";
                card_Payment = "";

                amountText.setText("SR 0");
                taxText.setText("SR 0");
                totalAmountText.setText("SR 0");

                adapterCarList = new AdapterCarList(MainActivity.this, images, new AdapterCarList.OnItemClickListener() {
                    @Override
                    public void onItemClick(int item) {

                        item1 = idArray1[item];

                        g1g2 = "";

                        g1g2 = "1" + item1 + item2;

                        if (!item2.isEmpty()) {
                            for (int i = 0; i < apiModelArrayList.size(); i++) {
                                if (g1g2.equals(apiModelArrayList.get(i).getId())) {

                                    if (detailArrayList.isEmpty()) {
                                        ItemListModel itemListModel = new ItemListModel();
                                        itemListModel.setQuantity("1");
                                        itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                                        itemListModel.setSr_no("1");
                                        itemListModel.setId(g1g2);
                                        itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                                        Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                                        itemListModel.setPrice(String.valueOf(finalPrice));

                                        detailArrayList.add(itemListModel);
                                    } else {
                                        ItemListModel itemListModel = new ItemListModel();
                                        itemListModel.setQuantity("1");
                                        itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                                        itemListModel.setSr_no("1");
                                        itemListModel.setId(g1g2);
                                        itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                                        Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                                        itemListModel.setPrice(String.valueOf(finalPrice));

                                        detailArrayList.set(0, itemListModel);
                                    }


                                    listSizeText.setText(String.valueOf(detailArrayList.size()));
                                    totalItemsBottom.setText(String.valueOf(detailArrayList.size()));


                                    totalTaxAmount = 0;
                                    for (int j = 0; j < detailArrayList.size(); j++){
                                        totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                                    }
                                    Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                    totalAmountText.setText("SR " + rounded);

                                    amount = 0;
                                    amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                                    Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                    amountText.setText("SR " + rounded1);

                                    Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                    taxText.setText("SR " + roundedTax);
                                }

                            }
                        }

                        adapterItemsList.notifyDataSetChanged();
                    }
                });
                recycler1.setAdapter(adapterCarList);
                recycler1.setHasFixedSize(true);

                adapterCarList = new AdapterCarList(MainActivity.this, serviceImage, new AdapterCarList.OnItemClickListener() {
                    @Override
                    public void onItemClick(int item) {

                        if (item1.isEmpty()){
                            Toast.makeText(MainActivity.this, "Please select Group 1!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        item2 = idArray2[item];

                        g1g2 = "";
                        g1g2 = "1" + item1 + item2;

                        for (int i = 0; i < apiModelArrayList.size(); i++){
                            if (g1g2.equals(apiModelArrayList.get(i).getId())){

                                if (detailArrayList.isEmpty()) {
                                    ItemListModel itemListModel = new ItemListModel();
                                    itemListModel.setQuantity("1");
                                    itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                                    itemListModel.setSr_no("1");
                                    itemListModel.setId(g1g2);
                                    itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                                    Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                                    itemListModel.setPrice(String.valueOf(finalPrice));

                                    detailArrayList.add(itemListModel);
                                } else {
                                    ItemListModel itemListModel = new ItemListModel();
                                    itemListModel.setQuantity("1");
                                    itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                                    itemListModel.setSr_no("1");
                                    itemListModel.setId(g1g2);
                                    itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());

                                    Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                                    itemListModel.setPrice(String.valueOf(finalPrice));

                                    detailArrayList.set(0, itemListModel);
                                }

                                totalTaxAmount = 0;
                                for (int j = 0; j < detailArrayList.size(); j++){
                                    totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                                }
                                Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                totalAmountText.setText("SR " + rounded);

                                amount = 0;
                                amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                                Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                amountText.setText("SR " + rounded1);

                                Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                taxText.setText("SR " + roundedTax);


                                listSizeText.setText(String.valueOf(detailArrayList.size()));
                                totalItemsBottom.setText(String.valueOf(detailArrayList.size()));

                            }
                        }

                        adapterItemsList.notifyDataSetChanged();
                    }
                });
                recycler2.setAdapter(adapterCarList);

                for (int i = 0; i < serviceModelArrayList3.size(); i++){
                    serviceModelArrayList3.get(i).setIsSelect("false");
                }

                adapterList3 = new AdapterList3(MainActivity.this, serviceImage3, serviceModelArrayList3, new AdapterList3.OnItemClickListener() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onItemClick(int item) {

                        if (item2.isEmpty()){
                            Toast.makeText(MainActivity.this, "Please select Group 2!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (serviceModelArrayList3.get(item).getIsSelect().equals("false")) {
                            serviceModelArrayList3.get(item).setIsSelect("true");

                            for (int i = 0; i < apiModelArrayList.size(); i++){

                                if (idArray3[item].equals(apiModelArrayList.get(i).getId())){
                                    ItemListModel itemListModel = new ItemListModel();
                                    itemListModel.setQuantity("1");
                                    itemListModel.setName(apiModelArrayList.get(i).getNameEn());
                                    itemListModel.setSr_no(String.valueOf(detailArrayList.size() + 1));
                                    itemListModel.setId(idArray3[item]);
                                    itemListModel.setBase_price(apiModelArrayList.get(i).getPrice());


                                    Float finalPrice = 1 * (Float.parseFloat(apiModelArrayList.get(i).getPrice()) * ( 1 + (Float.parseFloat(tax))/100));
                                    itemListModel.setPrice(String.valueOf(finalPrice));

                                    detailArrayList.add(itemListModel);
                                }
                            }


                        } else {
                            serviceModelArrayList3.get(item).setIsSelect("false");

                            for (int i = 0; i < detailArrayList.size(); i++){
                                if (idArray3[item].equals(detailArrayList.get(i).getId())){
                                    detailArrayList.remove(i);

                                    for (int j = 0; j < detailArrayList.size(); j++){
                                        detailArrayList.get(j).setSr_no(String.valueOf(j+1));
                                    }
                                }
                            }

                        }

                        listSizeText.setText(String.valueOf(detailArrayList.size()));
                        totalItemsBottom.setText(String.valueOf(detailArrayList.size()));


                        totalTaxAmount = 0;
                        for (int j = 0; j < detailArrayList.size(); j++){
                            totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                        }
                        Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        totalAmountText.setText("SR " + rounded);

                        amount = 0;
                        amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                        Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        amountText.setText("SR " + rounded1);

                        Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        taxText.setText("SR " + roundedTax);

                        adapterItemsList.notifyDataSetChanged();
                        adapterList3.notifyDataSetChanged();

                    }
                    @Override
                    public void onItemClickPlus(int position, String quantity) {

                        if (position != 1 && position != 3) {
                            serviceModelArrayList3.get(position).setQuantity(quantity);

                            for (int i = 0; i < detailArrayList.size(); i++){
                                if (idArray3[position].equals(detailArrayList.get(i).getId())){
                                    detailArrayList.get(i).setQuantity(quantity);

                                    Float finalPrice = Float.parseFloat(quantity) *
                                            (Float.parseFloat(detailArrayList.get(i).getBase_price()) * (1 + (Float.parseFloat(tax))/100));

                                    detailArrayList.get(i).setPrice(String.valueOf(finalPrice));

                                    Log.e("quanData", quantity + "--" + detailArrayList.get(i).getPrice());

                                    adapterItemsList.notifyDataSetChanged();
                                }
                            }

                        }

                        totalTaxAmount = 0;
                        for (int j = 0; j < detailArrayList.size(); j++){
                            totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                        }
                        Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        totalAmountText.setText("SR " + rounded);

                        amount = 0;
                        amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                        Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        amountText.setText("SR " + rounded1);

                        Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        taxText.setText("SR " + roundedTax);
                    }

                    @Override
                    public void onItemClickMinus(int position, String quantity) {
                        serviceModelArrayList3.get(position).setQuantity(quantity);

                        if (position != 1 && position != 3) {
                            serviceModelArrayList3.get(position).setQuantity(quantity);

                            for (int i = 0; i < detailArrayList.size(); i++){
                                if (idArray3[position].equals(detailArrayList.get(i).getId())){
                                    detailArrayList.get(i).setQuantity(quantity);

                                    Float finalPrice = Float.parseFloat(quantity) *
                                            (Float.parseFloat(detailArrayList.get(i).getBase_price()) * (1 + (Float.parseFloat(tax))/100));

                                    detailArrayList.get(i).setPrice(String.valueOf(finalPrice));

                                    Log.e("quanData", quantity + "--" + detailArrayList.get(i).getBase_price());

                                    adapterItemsList.notifyDataSetChanged();
                                }
                            }
                        }

                        totalTaxAmount = 0;
                        for (int j = 0; j < detailArrayList.size(); j++){
                            totalTaxAmount = totalTaxAmount + Float.parseFloat(detailArrayList.get(j).getPrice());
                        }
                        Double rounded= new BigDecimal(totalTaxAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        totalAmountText.setText("SR " + rounded);

                        amount = 0;
                        amount = totalTaxAmount / (1 + (Float.parseFloat(tax) / 100));
                        Double rounded1= new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        amountText.setText("SR " + rounded1);

                        Double roundedTax= new BigDecimal(String.valueOf(rounded - rounded1)).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        taxText.setText("SR " + roundedTax);
                    }

                    @Override
                    public void onClickGone(int position, String quantity) {
                        serviceModelArrayList3.get(position).setQuantity(quantity);
                    }

                });
                recycler3.setAdapter(adapterList3);
                adapterList3.notifyDataSetChanged();


            }
        }, secondsDelayed * 1000);

      /*  String stBulder = "";

        for (int i = 0; i < listOfServicesBill.size(); i++){
            stBulder = stBulder + (listOfServicesBill.get(i).getServiceName() + "   " + listOfServicesBill.get(i).getQty() + "   " +
                    listOfServicesBill.get(i).getTotalPrice() + "\n\n");
        }


        Bitmap icon;

        if (logoBill.equals("Extocare")){
            icon = BitmapFactory.decodeResource(getResources(), R.drawable.print_icon);
        } else {
            icon = BitmapFactory.decodeResource(getResources(), R.drawable.print_icon_main);
        }

        byte[] decodedString = Base64.decode(qrCodeBill, Base64.DEFAULT);
        Bitmap qrCodeBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        *//*bitMapImage = findViewById(R.id.bitMapImage);
        bitMapImage.setImageBitmap(icon);*//*


        String group2 =
                "\n\n" +companyNameBill + "\n\n" +
                        branchAddressBill + "\n\n" +
                        " الرقم الضريبي :  " + companyVatNumberBill + " :Vat" + "\n\n" + "\n" +
                        " رقم الفاتورة :  " + billNoBill + " :BillNo"+ "\n\n" + "\n" ;

        String group4 =
                " التاريخ : " + dateBill + " :Date" + "\n\n" + "\n" +
                        " الوقت : " + timeBill + " :Time" + "\n\n" + "\n" +
                        " اسم الكاشير  " + cashiermanBill + "\n\n" + "\n" +
                        "----------------------------" + "\n\n" + "\n" +
                        stBulder +
                        "----------------------------" + "\n\n" + "\n" +
                        " طريقة الدفع : " + paymentMethodBill + "\n\n" + "\n" +
                        " المبلغ الإجمالي : " + amountBill + "\n\n" + "\n" +
                        " الضريبة المضافة : " + taxBill + "\n\n" + "\n" +
                        " الإجمالي شامل الضريبة المضافة : " + totalBill + "\n\n" + "\n" +
                        "----------------------------" + "\n\n" + "\n" +
                        " " + branchEmailBill + "\n\n" + "\n" +
                        " " + branchWebsiteBill + "\n\n" + "\n" +
                        " " + branchPhoneBill + "\n\n" + "\n" +
                        " " + " سعدنا بزيارتكم " + "\n" + "\n" + "\n" + "\n";





        byte[] bytes = group2.getBytes("UTF-8");
        byte[] bytes1 = group4.getBytes("UTF-8");


        String str1 = new String(bytes, StandardCharsets.UTF_8); // for UTF-8 encoding
        String str2 = new String(bytes1, StandardCharsets.UTF_8); // for UTF-8 encoding


        Log.e("Final_Result",str1+str2);


        sendData(icon,bytes,qrCodeBitmap,bytes1);

*/

    }





    public void initializeAll(){
        mConnectBtn			= (Button) findViewById(R.id.btn_connect);
        mEnableBtn			= (Button) findViewById(R.id.btn_enable);
        btnPrint 		= (TextView) findViewById(R.id.btnPrint);
        mDeviceSp 			= (Spinner) findViewById(R.id.sp_device);
        mBluetoothAdapter	= BluetoothAdapter.getDefaultAdapter();
    }


    private void getPrint() {

        final KProgressHUD progressDialog = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        Map<String, Object> postParam= new HashMap<String, Object>();
        postParam.put("amount", Float.parseFloat(amountText.getText().toString().replace("SR ", "")));
        postParam.put("tax", Float.parseFloat(taxText.getText().toString().replace("SR ", "")));
        postParam.put("total", Float.parseFloat(totalAmountText.getText().toString().replace("SR ", "")));
        postParam.put("paymentMethod", type_Payment);
        postParam.put("cash", Float.parseFloat(cash_Payment));
        postParam.put("creditCard", Float.parseFloat(card_Payment));

        JSONObject object = new JSONObject(postParam);
        JSONArray array = new JSONArray();

        for(int i = 0; i < detailArrayList.size(); i++){
            JSONObject obj = new JSONObject();
            try {
                obj.put("id",Float.parseFloat(detailArrayList.get(i).getId()));
                obj.put("is_countable", true);
                obj.put("price",Float.parseFloat(detailArrayList.get(i).getPrice()));
                obj.put("qty",Float.parseFloat(detailArrayList.get(i).getQuantity()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            array.put(obj);
        }

        try {
            object.put("listOfServices",array);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, session.BASEURL + "SalesMain", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        listOfServicesBill.clear();
                        progressDialog.dismiss();

                        Log.e("responceFinalPrint", response.toString());

                        try {
                            JSONObject object1 = response.getJSONObject("bill");

                            logoBill = object1.getString("logo");
                            companyNameBill = object1.getString("companyName");
                            branchAddressBill = object1.getString("branchAddress");
                            companyVatNumberBill = object1.getString("companyVatNumber");
                            qrCodeBill = object1.getString("qrCode");
                            billNoBill = object1.getString("billNo");
                            dateBill = object1.getString("date");
                            timeBill = object1.getString("time");
                            cashiermanBill = object1.getString("cashierman");
                            paymentMethodBill = object1.getString("paymentMethod");
                            amountBill = object1.getString("amount");
                            taxBill = object1.getString("tax");
                            totalBill = object1.getString("total");
                            branchEmailBill = object1.getString("branchEmail");
                            branchWebsiteBill = object1.getString("branchWebsite");
                            branchPhoneBill = object1.getString("branchPhone");

                            JSONArray jsonArray = object1.getJSONArray("listOfServices");
                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject object2 = jsonArray.getJSONObject(i);

                                BillModel billModel = new BillModel();
                                billModel.setServiceName(object2.getString("serviceName"));
                                billModel.setQty(object2.getString("qty"));
                                billModel.setTotalPrice(object2.getString("totalPrice"));

                                listOfServicesBill.add(billModel);
                            }

                            printMessage();




                        } catch (JSONException | UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", "Error: " + error.getMessage());
                Toast.makeText(MainActivity.this,"Error Code: " + error.networkResponse.statusCode,Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + session.getAPIToken());
                return headers;
            }
        };

        jsonObjReq.setTag("TAG");
        // Adding request to request queue
        mRequestqueue.add(jsonObjReq);

        // Cancelling request
    /* if (queue!= null) {
    queue.cancelAll(TAG);
    } */

    }


    private void getAllData() {

        final KProgressHUD progressDialog = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        Map<String, String> postParam= new HashMap<String, String>();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, session.BASEURL + "services", new JSONObject(postParam),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.toString()));

                            JSONArray jsonArray = jsonObject.getJSONArray("services");

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject object = jsonArray.getJSONObject(i);

                                ApiModel apiModel = new ApiModel();
                                apiModel.setId(object.getString("id"));
                                apiModel.setNameAr(object.getString("nameAr"));
                                apiModel.setNameEn(object.getString("nameAr"));
                                apiModel.setPrice(object.getString("price"));
                                apiModel.setIsCountable(object.getString("isCountable"));

                                apiModelArrayList.add(apiModel);

                         //       Log.e("CheckData", apiModelArrayList.get(i).getId() + "--");
                            }

                            tax = jsonObject.getJSONObject("vat").getString("percentage");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("TAG", response.toString());



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", "Error: " + error.getMessage());
                Toast.makeText(MainActivity.this,"Error Code: " + error.networkResponse.statusCode,Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + session.getAPIToken());
                return headers;
            }
        };

        jsonObjReq.setTag("TAG");
        // Adding request to request queue
        mRequestqueue.add(jsonObjReq);

        // Cancelling request
    /* if (queue!= null) {
    queue.cancelAll(TAG);
    } */

    }


    private void openDialog() {
        Dialog dialogForCity = new Dialog(MainActivity.this);
        dialogForCity.setContentView(R.layout.custom_dialog_bill);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView dialogTotal = dialogForCity.findViewById(R.id.dialogTotal);
        TextView dialogCash = dialogForCity.findViewById(R.id.dialogCash);
        TextView dialogCard = dialogForCity.findViewById(R.id.dialogCard);


        dialogTotal.setText(totalAmountText.getText().toString().replace("SR", "") + " SR");

        Float value = Float.parseFloat(totalAmountText.getText().toString().replace("SR", ""));
        Double val1= new BigDecimal(value).setScale(2, RoundingMode.HALF_UP).doubleValue();

    //    String val1= new DecimalFormat("##.##").format(value);

        Log.e("dffff", new DecimalFormat("##.##").format(value) +"--");



        Float partsTwo = Float.parseFloat(String.valueOf(val1))/2;
        String rounded1 = new DecimalFormat("##.##").format(partsTwo);

        dialogCash.setText(String.valueOf(rounded1));
        dialogCard.setText(String.valueOf(rounded1));

        cash_Payment = dialogCash.getText().toString();
        card_Payment = dialogCard.getText().toString();

        Log.e("balance", Float.parseFloat(cash_Payment) + "--" + Float.parseFloat(card_Payment) + "--"+ Float.parseFloat(String.valueOf(val1)));


        dialogForCity.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogForCity.dismiss();
            }
        });

        dialogForCity.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cash_Payment = dialogCash.getText().toString();
                card_Payment = dialogCard.getText().toString();

                Float ttl_amount = Float.parseFloat(cash_Payment) + Float.parseFloat(card_Payment);
                Double rounded= new BigDecimal(ttl_amount).setScale(2, RoundingMode.HALF_UP).doubleValue();

                Log.e("balance", cash_Payment + "--" + card_Payment + "--"+ val1
                + "--ttamt  " + rounded);


                if (val1.toString().equals(rounded.toString())) {
                    if (dialogCash.getText().toString().equals("0") || dialogCard.getText().toString().equals("0")) {
                        Toast.makeText(dialogForCity.getContext(), "Please enter some amount!", Toast.LENGTH_SHORT).show();
                    } else {
                        dialogForCity.dismiss();
                        cash_Payment = dialogCash.getText().toString();
                        card_Payment = dialogCard.getText().toString();

                    }
                } else {
                    Toast.makeText(MainActivity.this, "Amount are not equal to Total amount!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        dialogForCity.show();
    }


    @Override
    public void onPause() {
        if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.cancelDiscovery();
            }
        }

        if (mConnector != null) {
            try {
                mConnector.disconnect();
            } catch (P25ConnectionException e) {
                e.printStackTrace();
            }
        }

        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);

        super.onDestroy();
    }

    private String[] getArray(ArrayList<BluetoothDevice> data) {

        String[] list = new String[0];

        if (data == null) return list;

        int size	= data.size();
        list		= new String[size];

        for (int i = 0; i < size; i++) {
            Log.e("Name",data.get(i).getName());

            list[i] = data.get(i).getName();
        }

        return list;
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void updateDeviceList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, getArray(mDeviceList));

        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        mDeviceSp.setAdapter(adapter);

    }

    private void showDisabled() {
        showToast("Bluetooth disabled");

        mEnableBtn.setVisibility(View.VISIBLE);
        mConnectBtn.setVisibility(View.GONE);
        mDeviceSp.setVisibility(View.GONE);
    }

    private void showEnabled() {
        showToast("Bluetooth enabled");

        mEnableBtn.setVisibility(View.GONE);
        mConnectBtn.setVisibility(View.VISIBLE);
        mDeviceSp.setVisibility(View.VISIBLE);
    }

    private void showUnsupported() {
        showToast("Bluetooth is unsupported by this device");

        mConnectBtn.setEnabled(false);
        btnPrint.setEnabled(true);
        mDeviceSp.setEnabled(false);
    }

    private void showConnected() {
        showToast("Connected");

        mConnectBtn.setText("Disconnect");

        btnPrint.setEnabled(true);

        mDeviceSp.setEnabled(false);
    }

    private void showDisonnected() {
        showToast("Disconnected");

        mConnectBtn.setText("Connect");

        btnPrint.setEnabled(true);
        mDeviceSp.setEnabled(true);
    }

    private void connect() {
        if (mDeviceList == null || mDeviceList.size() == 0) {
            return;
        }


        BluetoothDevice device = mDeviceList.get(mDeviceSp.getSelectedItemPosition());

        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
            try {
                createBond(device);
            } catch (Exception e) {
                showToast("Failed to pair device");

                return;
            }
        }

        try {
            if (!mConnector.isConnected()) {
                mConnector.connect(device);
            } else {
                mConnector.disconnect();

                showDisonnected();
            }
        } catch (P25ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void createBond(BluetoothDevice device) throws Exception {

        try {
            Class<?> cl 	= Class.forName("android.bluetooth.BluetoothDevice");
            Class<?>[] par 	= {};

            Method method 	= cl.getMethod("createBond", par);

            method.invoke(device);

        } catch (Exception e) {
            e.printStackTrace();

            throw e;
        }
    }

    private void sendData(Bitmap mGroup1, byte[] mGroup2,Bitmap mGroup3,byte[] mGroup4) {
        try {
            mConnector.sendData(mGroup1,mGroup2,mGroup3,mGroup4);
        } catch (P25ConnectionException e) {
            e.printStackTrace();
        }
    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state 	= intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_ON) {
                    showEnabled();
                } else if (state == BluetoothAdapter.STATE_OFF) {
                    showDisabled();
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                mDeviceList = new ArrayList<BluetoothDevice>();

                mProgressDlg.show();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                mProgressDlg.dismiss();

                updateDeviceList();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                mDeviceList.add(device);

                showToast("Found device " + device.getName());
            } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED) {
                    showToast("Paired");

                    connect();
                }
            }
        }
    };




}