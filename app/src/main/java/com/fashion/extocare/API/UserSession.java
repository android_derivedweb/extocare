package com.fashion.extocare.API;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public String BASEURL = "https://kosoormobapp.azurewebsites.net/";

    private static final String PREF_NAME = "UserSessionPref_extocareV1.2";


    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String PR_ID = "pr_id";
    private static final String PR_NAMEAR = "pr_nameAr";
    private static final String PR_NAMEEN = "pr_nameEn";
    private static final String PR_MOBILE = "pr_mobile";
    private static final String BR_ID = "br_id";
    private static final String BR_NAME = "br_name";
    private static final String BR_VATNUMBER = "br_vat_number";
    private static final String BR_LOCATION = "br_location";
    private static final String BR_DISPLAYNAME = "br_display_name";
    private static final String BR_EMAIL = "br_email";
    private static final String BR_WEBSITE = "br_website";
    private static final String BR_CONTACT = "br_contact";
    private static final String BR_CITY = "br_city";
    private static final String CP_ID = "cp_id";
    private static final String CP_NAME = "cp_name";
    private static final String CP_DISPLAYNAMEAR = "cp_displayNameAr";
    private static final String TOKEN = "token";





    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }


    public void createLoginSession(String mPR_ID,
                                   String mPR_NAMEAR,
                                   String mPR_NAMEEN,
                                   String mPR_MOBILE,
                                   String mBR_ID,
                                   String mBR_NAME,
                                   String mBR_VATNUMBER,
                                   String mBR_LOCATION,
                                   String mBR_DISPLAYNAME,
                                   String mBR_EMAIL,
                                   String mBR_WEBSITE,
                                   String mBR_CONTACT,
                                   String mBR_CITY,
                                   String mCP_ID,
                                   String mCP_NAME,
                                   String mCP_DISPLAYNAMEAR,
                                   String mTOKEN
    ) {
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(PR_ID, mPR_ID);
        editor.putString(PR_NAMEAR, mPR_NAMEAR);
        editor.putString(PR_NAMEEN, mPR_NAMEEN);
        editor.putString(PR_MOBILE, mPR_MOBILE);
        editor.putString(BR_ID, mBR_ID);
        editor.putString(BR_NAME, mBR_NAME);
        editor.putString(BR_VATNUMBER, mBR_VATNUMBER);
        editor.putString(BR_LOCATION, mBR_LOCATION);
        editor.putString(BR_DISPLAYNAME, mBR_DISPLAYNAME);
        editor.putString(BR_EMAIL, mBR_EMAIL);
        editor.putString(BR_WEBSITE, mBR_WEBSITE);
        editor.putString(BR_CONTACT, mBR_CONTACT);
        editor.putString(BR_CITY, mBR_CITY);
        editor.putString(CP_ID, mCP_ID);
        editor.putString(CP_NAME, mCP_NAME);
        editor.putString(CP_DISPLAYNAMEAR, mCP_DISPLAYNAMEAR);
        editor.putString(TOKEN, mTOKEN);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }



    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }



    public String getAPIToken() {
        return sharedPreferences.getString(TOKEN, "");
    }





}
