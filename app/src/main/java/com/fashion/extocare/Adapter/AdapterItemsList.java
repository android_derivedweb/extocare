package com.fashion.extocare.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.extocare.Model.ItemListModel;
import com.fashion.extocare.R;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class AdapterItemsList extends RecyclerView.Adapter<AdapterItemsList.Viewholder> {

    private Context context;
    private ArrayList<ItemListModel> itemListModelArrayList = new ArrayList<>();

    public AdapterItemsList(Context context, ArrayList<ItemListModel> itemListModelArrayList) {
        this.context = context;
        this.itemListModelArrayList = itemListModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_items_list, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        Double rounded= new BigDecimal(Double.parseDouble(itemListModelArrayList.get(position).getPrice())).setScale(2, RoundingMode.HALF_UP).doubleValue();
        holder.price.setText("SR " + rounded);

        holder.sr_no.setText(itemListModelArrayList.get(position).getSr_no());
        holder.name.setText(itemListModelArrayList.get(position).getName());
        holder.quantity.setText(itemListModelArrayList.get(position).getQuantity());

        if(position==0){
            holder.sr_no.setBackground(context.getResources().getDrawable(R.drawable.round_gray_item));
            holder.name.setBackground(context.getResources().getDrawable(R.drawable.round_white_item));
            holder.price.setBackground(context.getResources().getDrawable(R.drawable.round_white_item_2));
        }else {
            holder.sr_no.setBackground(context.getResources().getDrawable(R.color.lightdark_gray));
            holder.name.setBackground(context.getResources().getDrawable(R.color.white));
            holder.price.setBackground(context.getResources().getDrawable(R.color.white));
        }


    }

    @Override
    public int getItemCount() {
        return itemListModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView sr_no, name, quantity, price;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            sr_no = itemView.findViewById(R.id.sr_no);
            name = itemView.findViewById(R.id.name);
            quantity = itemView.findViewById(R.id.quantity);
            price = itemView.findViewById(R.id.price);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
