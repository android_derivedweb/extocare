package com.fashion.extocare.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.extocare.Activity.MainActivity;
import com.fashion.extocare.R;


public class AdapterCarList extends RecyclerView.Adapter<AdapterCarList.Viewholder> {

    private final OnItemClickListener listener;
    private Context context;
    private int selectedPosition = -1;
    private int[] images;


    public AdapterCarList(Context context, int[] images, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.images = images;
        this.listener = onItemClickListener;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_car_list, null);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onItemClick(position);

                if (!MainActivity.item1.isEmpty()) {
                    if (selectedPosition >= 0)
                        notifyItemChanged(selectedPosition);
                    selectedPosition = holder.getAdapterPosition();
                    notifyItemChanged(selectedPosition);
                }

            }
        });

        if (selectedPosition == position) {
            holder.itemView.setSelected(true);
            holder.layout.setBackground(context.getResources().getDrawable(R.drawable.round_gray_10));
        } else {
            holder.itemView.setSelected(false);
            holder.layout.setBackground(context.getResources().getDrawable(R.drawable.round_gray_10_border));
        }

        Glide.with(context).load(images[position]).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        LinearLayout layout;
        ImageView image;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.layout);
            image = itemView.findViewById(R.id.image);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
