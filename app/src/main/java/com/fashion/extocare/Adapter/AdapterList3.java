package com.fashion.extocare.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.extocare.Activity.MainActivity;
import com.fashion.extocare.Model.ServiceModel3;
import com.fashion.extocare.R;

import java.util.ArrayList;


public class AdapterList3 extends RecyclerView.Adapter<AdapterList3.Viewholder> {

    private final OnItemClickListener listener;
    private Context context;
    private int[] images;
    private ArrayList<ServiceModel3> serviceModelArrayList;
    private int quantity = 1;


    public AdapterList3(Context context, int[] images, ArrayList<ServiceModel3> serviceModelArrayList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.images = images;
        this.serviceModelArrayList = serviceModelArrayList;
        this.listener = onItemClickListener;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_list3, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onItemClick(position);

               /* if (selectedPosition >= 0)
                    notifyItemChanged(selectedPosition);
                selectedPosition = holder.getAdapterPosition();
                notifyItemChanged(selectedPosition);*/
            }
        });

        /*if (selectedPosition == position) {
            holder.layout.setBackground(context.getResources().getDrawable(R.drawable.round_gray_10));
            holder.linearLayoutAdd.setVisibility(View.VISIBLE);
        } else {
            holder.layout.setBackground(context.getResources().getDrawable(R.drawable.round_gray_10_border));
            holder.linearLayoutAdd.setVisibility(View.GONE);
        }*/

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                increment(holder, position);
                listener.onItemClickPlus(position, holder.count.getText().toString());

            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                decrement(holder);
                listener.onItemClickMinus(position, holder.count.getText().toString());

            }
        });

        if (!MainActivity.item2.isEmpty()) {

            if (serviceModelArrayList.get(position).getIsSelect().equals("true")) {
                holder.layout.setBackground(context.getResources().getDrawable(R.drawable.round_gray_10));
                holder.linearLayoutAdd.setVisibility(View.VISIBLE);
            } else {
                holder.layout.setBackground(context.getResources().getDrawable(R.drawable.round_gray_10_border));
                holder.linearLayoutAdd.setVisibility(View.GONE);
                gone(holder, position);
                listener.onClickGone(position, holder.count.getText().toString());
            }

        }
        Glide.with(context).load(images[position]).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return serviceModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        LinearLayout layout, linearLayoutAdd;
        ImageView image, minus, plus;
        TextView count;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.layout);
            image = itemView.findViewById(R.id.image);
            linearLayoutAdd = itemView.findViewById(R.id.linearLayoutAdd);
            minus = itemView.findViewById(R.id.minus);
            plus = itemView.findViewById(R.id.plus);
            count = itemView.findViewById(R.id.count);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
        void onItemClickPlus(int position, String quantity);
        void onItemClickMinus(int position, String quantity);
        void onClickGone(int position, String quantity);
    }

    public void increment(Viewholder myViewHolder, int position) {

        if (position != 1 && position != 3) {
            quantity = Integer.parseInt(myViewHolder.count.getText().toString());

       /* if (Integer.parseInt(myViewHolder.txtCountProducts.getText().toString()) < Integer.parseInt(categoryDetailsModels.get(position).getAvailable_stock_qty())) {
            quantity++;
        }*/
            quantity++;
            myViewHolder.count.setText(String.valueOf(quantity));
        }

    }

    public void decrement(Viewholder myViewHolder) {
        if(!myViewHolder.count.getText().toString().equals("1")){
            quantity = Integer.parseInt(myViewHolder.count.getText().toString());
            quantity--;
            myViewHolder.count.setText(String.valueOf(quantity));
        }
    }

    public void gone(Viewholder myViewHolder, int position) {

        myViewHolder.count.setText("1");

    }


}
